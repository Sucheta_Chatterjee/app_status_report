import os
import csv
import epoch

import tkinter as tk
from tkinter import *

import requests
from requests.exceptions import HTTPError

"""
	@name: check_status
	@params: None
	@description: This method will check the status of application and generate a report
"""
def check_status():
	# initialization
	row_list = []

	# getting file as input
	file_path = mystring.get()
	
	try:
	    if os.path.exists(file_path) and os.access(file_path, os.R_OK):
	    	with open(file_path) as file:
	    		csv_reader = csv.reader(file, delimiter=',')
	    		line_count = 0
	    		for row in csv_reader:
	    			if line_count == 0:
	    				col_fields = row
	    				print(col_fields)
	    				line_count += 1
	    			else:
	    				row_list.append(row)
	    				line_count += 1

	    	html_code = '<html><head><title>Application Status Report</title></head><body>'
	    	tb_head = '<table><tr>'

	    	for col in col_fields:
	    		tb_head += '<th>' +col+ '</th>'

	    	tb_col = tb_head+'<th>Status</th></tr>'
	    	tb_row_body = ''

	    	for row in row_list:
	    		print(row)
	    		tb_row = ''
	    		index = row[0]
	    		url = row[1]

	    		try:
	    			response = requests.get(url)

	    			# If the response was successful, no Exception will be raised
	    			status = response.status_code
	    			if status == 200:
	    				status = "success"
	    				bg_color = "#15bd09"
	    			else:
	    				status = "failed"
	    				bg_color = "#d93823"

	    			# create table
	    			tb_row = "<tr style='background-color:"+bg_color+"'><td>"+index+"</td><td>"+url+"</td><td>"+status+"</td></tr>"
	    			tb_row_body += tb_row

	    		except HTTPError as http_err:
	    			print("HTTP error occured : {0}".format(http_err))

	    	# writing into html
	    	html_end_code = "</table></body></html>"
	    	data = html_code + tb_col + tb_row_body + html_end_code

	    	# output file path
	    	ofile = os.path.dirname(file_path)+"/html_status_report_"+str(int(epoch.now()))+".html"

	    	f = open(ofile,'w')
	    	f.write(data)
	    	f.close()

	    	print("file path : "+ ofile)
	    	print("writing html complete")

	    	label_txt = "Status report is ready. Report file path : "+ofile
	    	tk.Label(window, text=label_txt).pack()	    

	except FileNotFoundError:
		print("File path does not exists.") 


if __name__ == '__main__':
	window = tk.Tk()

	# add window title
	window.title("Server Status Checker")
	# set geometry
	window.geometry("500x300")

	mystring =tk.StringVar(window)

	entry = tk.Entry(window, textvariable=mystring)
	entry.insert(INSERT, "Select CSV file here.")
	entry.pack()

	frame = tk.Frame(window)
	frame.pack()

	bottomframe = tk.Frame(window)
	bottomframe.pack( side = BOTTOM )

	bluebutton = tk.Button(frame, text="Check", fg="blue", command=check_status)
	bluebutton.pack( side = LEFT )

	blackbutton = tk.Button(bottomframe, text="Close", fg="black", command=window.quit)
	blackbutton.pack( side = BOTTOM)

	window.mainloop()
